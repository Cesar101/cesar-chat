Template.home.events({
    'submit .new-message': function (event) {
        event.preventDefault();

        var postText = event.currentTarget.children[0].firstElementChild.value;
        Collections.Posts.insert({
            name: postText,
            createdAt: new Date(),
        });
        event.currentTarget.children[0].firstElementChild.value = "";
    }
});
Template.home.helpers({
    listPost: function(){
        return Collections.Posts.find({});
    }
});